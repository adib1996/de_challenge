<div align="center">

  <img src="assets/tamara_logo.PNG" alt="logo" width="200" height="auto" />
  <h1>Tamara DE Challenge</h1>

</div>

<br />

<!-- Table of Contents -->
# :notebook_with_decorative_cover: Table of Contents

- [About the Project](#star2-about-the-project)
  * [Tech Stack](#space_invader-tech-stack)
- [Getting Started](#toolbox-getting-started)
  * [Prerequisites](#bangbang-prerequisites)
  * [Installation](#gear-installation)
  * [Deployment](#triangular_flag_on_post-deployment)
- [In Depth Project Dive](#eyes-usage)
- [Improvements/To Do](#compass-roadmap)
- [Contributing](#wave-contributing)
  * [Code of Conduct](#scroll-code-of-conduct)
- [SQL Queries](#grey_question-faq)
- [Challenges in the 'Challenge](#warning-license)
- [Initial Requirement](#gem-acknowledgements)

  

<!-- About the Project -->
## :star2: About the Project

This is an assignment given by the Tamara Engineering team to perform ETL processes on an given OLTP Database. The OLTP database tracks certains events and records a payload for each event containing datapoints. The datapoints are stored in JSON format. The current pipeline that is set flatten the JSON data structure to then be stored back to the SQL Database which then can be used as an OLAP Database to answer the below statements.  
1. Most Purchased Items sorted by date
2. Items that has contributed to late fees
3. Top 10 merchants based of new order values
4. Top 10 merchants based of canceled order values
5. Total late feels collected sorted by date

Further discussion on the Architecure and logic is discussed below.

<!-- TechStack -->
### :space_invader: Tech Stack

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://www.python.org">Python</a></li>
    <li><a href="https://www.sqlalchemy.org/">SQLAlchemy</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.mysql.com/">MySQL</a></li>
  </ul>
</details>

<details>
<summary>DevOps</summary>
  <ul>
    <li><a href="https://www.docker.com/">Docker</a></li>
  </ul>
</details>

<!-- Getting Started -->
## 	:toolbox: Getting Started

<!-- Prerequisites -->
### :bangbang: Prerequisites
Clone the project

```bash
  git clone https://github.com/Louis3797/awesome-readme-template.git
```

This project uses Docker to build the pipeline. Install Docker from the below URL

```bash
 https://docs.docker.com/desktop/
```

<!-- Installation -->
### :gear: Installation

Load the Database and activate the pipeline, once you are in the same folder as the docker-compose.yml file

```bash
  docker-compose up -d
```

:bangbang: WARNING :bangbang:
There is a possiblity that the pipeline might not run due to access control limitations set by your default MySQL settings.

If this is the case please run the below command manually

```bash
    pip install -r requirements.txt
    cd .\pipeline\
    python .\deconstructer.py 
```
   
<!-- Deployment -->
### :triangular_flag_on_post: Deployment

Once the Database is loaded, log in to the Database with your favourite SQL Database IDE with the below credentials

```bash
  Service: Mysql@localhost:3306
  User: root
  password: admin
```

<!-- Usage -->
## :eyes: In depth Dive about the assignment

The objective of the assignment was to derive tables nested inside another table. The end goal would be an analytical layer with these derived tables mapped to answer simple business logic. The below shows a very high level architecture of how the process would work

  <img src="./assets/High_Level_Architecture.png" alt="screenshot" />

A sample of a raw table along with its derived table can be seen below:
  <img src="./assets/Derived_Table.png" alt="screenshot" width="500" />

The table is derived from the Payload column which consists of a JSON data structure which is then flattened by the ETL pipeline and then inserted back to the DB as a new table labeled "CLND_OrderWasDisputed"

The approach was to use a lookup table which consists of unique events in the orders_events table.
Using the lookup table we can look through each event in the order_events table and pull out the payload for each domain.

  <img src="./assets/lookup.png" alt="screenshot" width="500" />


## :compass: Improvements/Further To-Do

* [x] Create SQL Queries for based on questions
* [ ] Set up ENV file for parameters
* [ ] Set up Source Change Detection upsert
* [ ] Set up K8 cluster for deployment


## :grey_question: SQL Queries

Can also be found in ./sql_queries

- Top 10 most purchased items by day, month, quarter, year.

By day:
```
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where cast(a.created_at as date) = '2020-10-29' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10
```

Result:
```
عسل سدر بلدي ملكي (نص كيلو) ( حجز للموسم السدر الجديد )
نوفو بريــــك بار
براوني ايت مي توكسيدو
خبز البروتين
بودرة تثبيت اتش دي من ايلف - شير
خافي عيوب كونسيلر ايج ريوند من ميبلين
مجموعة اضاءة قلو ميك اب من ميك اوفر 22 - GM001
#10430عباية كريب كلاسيك ببطانة مميزة بالكم بيج - 54
#1622عباية ملكي كلاسيك أسود بشك - 56
اظافر ترندي الكورية
```

By Month:
```
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where MONTH(a.created_at) = '10' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10
```

Result:
```
مناكير نورة بوعوض CLASSY
روج قلوس مويرا MOIRA GLOSS AFFAIR GLG
سماعة   AirPods pro من شركة موج مكس
مثبت ايلف ELF
POWER Official Jersey
ساعة T5S الذكية
عباية كاجوال بأزره وجيوب
مجموعة المعجزة الكوريه من سام باي مي
تونر  سويس لاين SWISS LINE WATER SHOCK   160ML
محدد شفاه مويرا MOIRA LIPLINERLEP
```

By Quarter:
```
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where QUARTER(a.created_at) = '4' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10
```

Result:
```
POWER Official Jersey
عباية كاجوال بأزره وجيوب
عبايه عملية
روج قلوس مويرا MOIRA GLOSS AFFAIR GLG
مثبت ايلف ELF
شبية ساعة ابل طبق الاصل اصدار 6
ساعة T5S الذكية
ايلاينر ذابالم the balm
قلم محدد شفاه من نورة بو عوض - روز وود
عبايه شموخ
```

By Year:
```
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where YEAR(cast(a.created_at as date)) = '2020' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10
```

Result:
```
عدسات لنس مي بقياس نظر
جاردن اوليان - المجموعه المغربيه المثاليه  1
عباية كاجوال بأزره وجيوب
POWER Official Jersey
جولدن روز - سو كيوت ميني ايلاينر
تونر  سويس لاين SWISS LINE WATER SHOCK   160ML
نقاب واسع بلوقو
عبايه شموخ
مناكير نورة بوعوض CLASSY
مجموعة المعجزة الكوريه من سام باي مي
```

</details>


## :warning: Challenges in the 'Challenge'

The below are few of the challenges faced when trying to flatten the payload column:

Uneven Quotations in objects
<br>
<br>
<img src="./assets/evidence.PNG" alt="evidence" width="500" />
<br>
<br>
Some of the objects in payload column had formatting issues where the Quotation marks were uneven, causing an issue when being pulled by the json parser.
<br>
<br>
<img src="./assets/Proof_LI.jpg" alt="proof_LI" width="500" />
<br>
This was handled by applying REGEX on where any trailing quotation marks were handled. Ideally this isn't the right approach as this should be handled by the source itself.
<br>
<br>
Since the object was created from JavaScript and parsed on as a string. The Python parser I've created was unable to read boolean data types such as false|true as False|True and Null as None. 
<br>
<br>
<img src="./assets/data_type.jpg" alt="data_type" width="500" />
<br>
<br>
This was easily handles with pandas.replace function


## :gem: Initial Requirement
### Tamara Data Engineer Coding Challenge

#### Requirements
Our data warehouse system was replicated from our application data (in real time). The payload for order events in our application is in json format which didn't optimize for analytics applications. So when we need query/aggregate fields in a JSON column, it will take a considerable time. And we can't add indexes to fields inside the JSON column to speed up the query.

So we need to create some pipelines to denormalize JSON fields in order_events tables. The new data structure should answer business questions by conducting simple queries:

- Top 10 most purchased items by day, month, quarter, year.
- Top 10 items that contributed most to the late fee.
- Top 10 merchants who have most new order value by day, month, quarter, year
- Top 10 merchants who have most canceled order value by day, month, quarter, year
- Total late fee amount collected by day, month, quarter, year.

---
#### Acceptance Criteria
- Setup project structure + Docker + code linting + mypy
- Storage:
Input: MySQL
Output: Mysql
- Answer all critical business questions

#### Good to have:
- Pipeline that support incremental update in real time
- Unit tests and integration tests for critical logic
- Architecture: Cloud native (K8s)
- Define CICD (github, gitlab,...)

