FROM python:3.9

RUN mkdir -p /app
WORKDIR /app
COPY ./pipeline/requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY ./pipeline/ /app/
