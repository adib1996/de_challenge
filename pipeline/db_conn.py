from multiprocessing import Event
from xml.dom.minidom import Text
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.sql import text, func
from sqlalchemy.orm import sessionmaker,relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Float, TIMESTAMP

def cnx():
    conn = create_engine('mysql+pymysql://root:admin@localhost:3306/tamara')
    cnx = conn.connect()
    return cnx
