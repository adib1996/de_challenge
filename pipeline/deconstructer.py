import pandas as pd
import json
import ast
from pandas.io.json import json_normalize
from db_conn import cnx
from table_converter import sql_table_creater
import re
import warnings
from lookup_table import lookup_table
warnings.filterwarnings("ignore")

#Establishing SQL Engine + Connection String
cnx = cnx()
lookup_table()

lookup_table = pd.read_sql("SELECT * FROM tamara.events_lookup_table", cnx)

# ------------ Functions to decontruct payloads --------------- #

#To handle cases where the structure is not deformed
def only_dict_OrderWasCreated(d):
    return ast.literal_eval(d)

def get_df_OrderWasCreated(df, col_name):
  return json_normalize(df[col_name].apply(only_dict_OrderWasCreated).tolist())#.add_prefix(prefix)

#handling deformed dfs
def only_dict(d):
    return ast.literal_eval(d[1:-1])

def get_df(df, col_name):
  return json_normalize(df[col_name].apply(only_dict).tolist())#.add_prefix(prefix)
  
#Breaking down of json in lists

def get_df_lists(df,col_name):
    items = []
    for i, row in df.iterrows():
        temp = json_normalize(row[col_name])
        temp['order_id'] = row['order_id']
        items.append(temp)
    final_df = pd.concat(items, ignore_index=True)
    return final_df


for idx,series in lookup_table.iterrows():
    table_name = series['event_tag']
    x = series['query']
    if table_name == 'OrderWasCreated':
        val = "SELECT * FROM tamara.order_events where event_name = '{}';".format(x)
        df = pd.read_sql(val, cnx)
        df['payload'] = df['payload'].str.replace("\\",'')
        df['payload'] = df['payload'].str.replace("%",'')
        df['payload'] = df['payload'].str.replace("null",'None')
        df['payload'] = df['payload'].str.replace("true",'True')
        df['payload'] = df['payload'].str.replace("false",'False')
        df['payload'] = df['payload'].str.replace('بيعية مطلية بالذهب "نو','')
        df['payload'] = df['payload'].str.replace('وردة طعيه فاخرة"','')
        df['payload'] = df['payload'].str.replace('"دريمي مسك"','')
        df['payload'] = df['payload'].str.replace('\"risk_assessment\"\: \[\]\,','')
        # df['payload'] =  [re.sub(r'\"sku\":(.\"(.*?\")\")*','', str(x)) for x in df['payload']]
        table_abbrv = table_name +'_List'
        derived_table = 'CLND_OrderWasCreated_Normalized'
        item_name = 'items'

        df2 = get_df_OrderWasCreated(df,'payload') 
        df_list = get_df_lists(df2,item_name)
        sql_table_creater(df_list,table_name,derived_table)
        last_row = df_list.index.values[-1]
        rows_read = len(df_list.index)
    
        sql = '''UPDATE tamara.events_lookup_table
         SET rows_read = {0}, last_index_val = {1} WHERE event_tag = '{2}';'''.format(rows_read,last_row,table_name)
        cnx.execute(sql)

 
    elif table_name == 'OrderWasCanceled' or table_name == 'OrderWasCaptured' or table_name == 'OrderWasRefunded':
        val = "SELECT * FROM tamara.order_events where event_name = '{}';".format(x)
        df = pd.read_sql(val, cnx)
        df['payload'] = df['payload'].str.replace("\\",'')
        df['payload'] = df['payload'].str.replace("%",'')
        df['payload'] = df['payload'].str.replace("null",'None')
        df['payload'] = df['payload'].str.replace("true",'True')
        df['payload'] = df['payload'].str.replace("false",'False')
        df['payload'] = df['payload'].str.replace('بيعية مطلية بالذهب "نو','')
        df['payload'] = df['payload'].str.replace('وردة طعيه فاخرة"','')
        df['payload'] = df['payload'].str.replace('"دريمي مسك"','')
        df['payload'] = df['payload'].str.replace('\"risk_assessment\"\: \[\]\,','')
        # df['payload'] =  [re.sub(r'\"sku\":(.\"(.*?\")\")*','', str(x)) for x in df['payload']]

        table_abbrv = table_name +'_List'
        derived_table = 'CLND_'+table_name + '_Normalized'
        item_name = 'items'
        df2 = get_df(df,'payload')
        df_list = get_df_lists(df2,item_name)
        sql_table_creater(df_list,table_name,derived_table)
        last_row = df_list.index.values[-1]
        rows_read = len(df_list.index)
    
        sql = '''UPDATE tamara.events_lookup_table
         SET rows_read = {0}, last_index_val = {1} WHERE event_tag = '{2}';'''.format(rows_read,last_row,table_name)
        cnx.execute(sql)


    else:
        val = "SELECT * FROM tamara.order_events where event_name = '{}';".format(x)
        df = pd.read_sql(val, cnx)
        df['payload'] = df['payload'].str.replace("\\",'')
        df['payload'] = df['payload'].str.replace("%",'')
        df['payload'] = df['payload'].str.replace("null",'None')
        df['payload'] = df['payload'].str.replace("true",'True')
        df['payload'] = df['payload'].str.replace("false",'False')
        df['payload'] = df['payload'].str.replace('بيعية مطلية بالذهب "نو','')
        df['payload'] = df['payload'].str.replace('وردة طعيه فاخرة"','')
        df['payload'] = df['payload'].str.replace('\"risk_assessment\"\: \[\]\,','')
        # df['payload'] =  [re.sub(r'\"sku\":(.\"(.*?\")\")*','', str(x)) for x in df['payload']]

        df2 = get_df(df,'payload')
        table_abbrv = 'CLND_'+table_name
        sql_table_creater(df2,table_name,table_abbrv)
        last_row = df2.index.values[-1]
        rows_read = len(df2.index)
    
        sql = '''UPDATE tamara.events_lookup_table
         SET rows_read = {0}, last_index_val = {1} WHERE event_tag = '{2}';'''.format(rows_read,last_row,table_name)
        cnx.execute(sql)



