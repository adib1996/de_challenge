import datetime
from multiprocessing import Event
from xml.dom.minidom import Text
import pandas as pd
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.sql import text, func
from sqlalchemy.orm import sessionmaker,relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Float, TIMESTAMP
from db_conn import cnx

cnx = cnx()

def lookup_table():
    sql = text('DROP TABLE IF EXISTS tamara.events_lookup_table;')
    result = cnx.execute(sql)

    Base = declarative_base()
    DBSession = sessionmaker(bind=cnx)
    session = DBSession()

    Base.metadata.create_all(cnx)

    class Student(Base):
        __tablename__ = "Events_Lookup_Table"
        event_id = Column(Integer, primary_key=True)
        event_name = Column(String(255))
        rows_read = Column(Integer)
        last_index_val = Column(Integer)
        lookup_query = Column(String(255))
        event_tag = Column(String(255))

        def __repr__(self):
            return "<Events_Lookup_Table(event_id='%s', event_name='%s', rows_read='%s',last_index_val='%s')>" % (self.event_id, self.event_name, self.event_name, self.rows_read, self.last_index_val)


    df = pd.read_sql('SELECT distinct(event_name) FROM tamara.order_events', cnx)
    query_list = []
    for x in df['event_name']:
        new_str = x.replace('\\','\\\\')
        query_list.append(new_str)


    event = []
    for y in df['event_name']:
        evn_tag = y.split('\\')[5]
        event.append(evn_tag)

    df['event_name'] = df['event_name'].str.strip()
    df['rows_read'] = 0
    df['last_index_val'] = 0
    df['query'] = query_list
    df['event_tag'] = event


    df.to_sql('events_lookup_table',con=cnx,if_exists='replace',index=False)

