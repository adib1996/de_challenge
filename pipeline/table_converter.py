import pandas as pd
from db_conn import cnx
from sqlalchemy import create_engine
import re
import sys

def sql_table_creater(df,event_tag,tablename):
    df = df
    event_tag = event_tag
    db = 'tamara'
    db_tbl_name = tablename

    def mysql_engine(user = 'root', password = 'admin', host = 'localhost', port = '3306', database = 'tamara'):
        engine = create_engine("mysql://{0}:{1}@{2}:{3}/{4}?charset=utf8".format(user, password, host, port, database),encoding='cp720')
        return engine

    def mysql_conn(engine):
        conn = engine.raw_connection() 
        return conn

    def csv_to_df(df, headers = []):
        if len(headers) == 0:
            df = df
        else:
            df.columns = headers
        for r in range(10):
            try:
                df.rename( columns={'Unnamed: {0}'.format(r):'Unnamed{0}'.format(r)},    inplace=True )
            except:
                pass
        return df

    def dtype_mapping():
        return {'object' : 'TEXT',
            'int64' : 'INT',
            'float64' : 'FLOAT',
            'datetime64' : 'DATETIME',
            'bool' : 'TINYINT',
            'category' : 'TEXT',
            'timedelta[ns]' : 'TEXT'}

    def gen_tbl_cols_sql(df):
        dmap = dtype_mapping()
        sql = "pi_db_uid INT AUTO_INCREMENT PRIMARY KEY"
        df.columns = [re.sub("[ ,-]", "_", re.sub("[\.,`,\$]", "", c)) for c in df.columns]
        df.columns = df.columns.str.replace(r"to", "to_D")
        df.columns = df.columns.str.replace(r"from", "from_D")
        df1 = df.rename(columns = {"" : "nocolname"})
        hdrs = df1.dtypes.index
        hdrs_list = [(hdr, str(df1[hdr].dtype)) for hdr in hdrs]
        for i, hl in enumerate(hdrs_list):
            sql += " ,{0} {1}".format(hl[0], dmap[hl[1]])
        return sql

    def create_mysql_tbl_schema(df, conn, db, tbl_name):
        tbl_cols_sql = gen_tbl_cols_sql(df)
        sql = "USE {0}; CREATE TABLE {1} ({2})".format(db, tbl_name, tbl_cols_sql)
        sql_check = "DROP TABLE IF EXISTS tamara.{};".format(tbl_name)
        cur = conn.cursor()
        cur.execute(sql_check)
        cur.execute(sql)
        cur.close()
        conn.commit()

    def df_to_mysql(df, engine, tbl_name):
        df.to_sql(tbl_name, engine, if_exists='replace')

    try:
        df = csv_to_df(df)
        create_mysql_tbl_schema(df, mysql_conn(mysql_engine()), db, db_tbl_name)
        df_to_mysql(df, mysql_engine(), db_tbl_name)
    except Exception as e:
        print(e)
