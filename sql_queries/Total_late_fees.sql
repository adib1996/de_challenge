
with cte1 as (
SELECT order_id,sum(late_fee_amountamount) as late_fee FROM tamara.CLND_OrderWasOverdue WHERE order_id
 NOT IN (SELECT order_id FROM tamara.CLND_LateFeeWasRemoved)
group by order_id
)
select cast(c.created_at as date) as created_at,sum(late_fee) as late_fee_total from cte1 a
inner join tamara.orders c
on a.order_id = c.id_order
group by c.created_at
order by sum(late_fee) desc
LIMIT 10

----- By YEar ----- 
with cte1 as (
SELECT order_id,sum(late_fee_amountamount) as late_fee FROM tamara.CLND_OrderWasOverdue WHERE order_id
 NOT IN (SELECT order_id FROM tamara.CLND_LateFeeWasRemoved)
group by order_id
)
select distinct(year(c.created_at)) as created_at,sum(late_fee) as late_fee_total from cte1 a
inner join tamara.orders c
on a.order_id = c.id_order
group by year(c.created_at)
order by sum(late_fee) desc
LIMIT 10