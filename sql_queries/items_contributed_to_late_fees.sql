-- Items that contributed most to late fees

-- CTE to exclude orders where late fee was removed
with cte1 as (
SELECT order_id,sum(late_fee_amountamount) as late_fee FROM tamara.CLND_OrderWasOverdue WHERE order_id
 NOT IN (SELECT order_id FROM tamara.CLND_LateFeeWasRemoved)
group by order_id
)

select distinct(c.name),sum(late_fee) as late_fee_total from cte1 a
inner join tamara.CLND_OrderWasCreated_Normalized c
on a.order_id = c.order_id
group by c.name
order by sum(late_fee) desc
LIMIT 10



