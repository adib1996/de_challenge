-- TOP 10 ITEMS ORDERED BY DAY -- 
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where cast(a.created_at as date) = '2020-10-29' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10

-- TOP 10 ORDERS BY MONTH --
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where MONTH(a.created_at) = '10' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10

-- TOP 10 ORDERS BY QUARTER -- 
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where QUARTER(a.created_at) = '4' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10

-- TOP 10 ORDERS BASED ON YEAR -- 
SELECT b.name,count(*) as purchase_count, sum(a.total_amount) as total_amount FROM tamara.orders a
inner join tamara.CLND_OrderWasCreated_Normalized b
on a.id_order = b.order_id
where YEAR(cast(a.created_at as date)) = '2020' and status not in ('expired','declined','canceled')
group by name
order by count(*) desc
LIMIT 10