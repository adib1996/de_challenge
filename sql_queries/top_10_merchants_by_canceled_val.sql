--- Cancelled Orders

-- By Day
select b.merchant_name,sum(b.total_amount) as total_amount from tamara.CLND_OrderWasCanceled_Normalized a
left join tamara.orders b
on a.order_id = b.id_order
where cast(b.created_at as date) = '2020-10-29'
group by b.merchant_name
order by sum(b.total_amount) desc
limit 10

-- By Month
select b.merchant_name,sum(b.total_amount) as total_amount from tamara.CLND_OrderWasCanceled_Normalized a
left join tamara.orders b
on a.order_id = b.id_order
where month(cast(b.created_at as date)) = '10'
group by b.merchant_name
order by sum(b.total_amount) desc
limit 10

-- By Quarter
select b.merchant_name,sum(b.total_amount) as total_amount from tamara.CLND_OrderWasCanceled_Normalized a
left join tamara.orders b
on a.order_id = b.id_order
where Quarter(cast(b.created_at as date)) = '4'
group by b.merchant_name
order by sum(b.total_amount) desc
limit 10

-- By Year
select b.merchant_name,sum(b.total_amount) as total_amount from tamara.CLND_OrderWasCanceled_Normalized a
left join tamara.orders b
on a.order_id = b.id_order
where Year(cast(b.created_at as date)) = '2020'
group by b.merchant_name
order by sum(b.total_amount) desc
limit 10