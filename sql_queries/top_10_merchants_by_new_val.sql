-- Top 10 Merchants with highest new order value
-- Order by Day
SELECT merchant_name, sum(total_amount) FROM tamara.orders
where cast(created_at as date) = '2020-10-29' and status not in ('expired','declined','canceled')
group by merchant_name
order by sum(total_amount) desc
limit 10

-- Order by Month
SELECT merchant_name, sum(total_amount) FROM tamara.orders
where month(cast(created_at as date)) = '10' and status not in ('expired','declined','canceled')
group by merchant_name
order by sum(total_amount) desc
limit 10

-- Order by Quarter
SELECT merchant_name, sum(total_amount) FROM tamara.orders
where Quarter(cast(created_at as date)) = '4' and status not in ('expired','declined','canceled')
group by merchant_name
order by sum(total_amount) desc
limit 10

-- Order by Year
SELECT merchant_name, sum(total_amount) FROM tamara.orders
where Year(cast(created_at as date)) = '2020' and status not in ('expired','declined','canceled')
group by merchant_name
order by sum(total_amount) desc
limit 10
